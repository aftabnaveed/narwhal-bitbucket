<?php

require_once './vendor/autoload.php';

use React\EventLoop\Factory;
use React\Socket\Server as SocketServer;
use React\Http\Server as HttpServer;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use Narwhal\Bitbucket\Bitbucket;
use Narwhal\Bitbucket\Exceptions\BitbucketException;

$loop = Factory::create();
$socket = new SocketServer('0.0.0.0:9000', $loop);

$http = new HttpServer(function(ServerRequestInterface $request) {

    try {
        $json = $request->getBody()->getContents();

        $bitbucket = new Bitbucket($json);
        $bitbucket->handle();

        return new Response(
            200,
            [ 'Content-Type' => 'text/plain'],
            "HelloWorld\n"
        );
    } catch(BitbucketException $e) {
        error_log($e->getMessage());
    }

    catch(\Throwable $e) {
        var_dump($e->getMessage());
    }
});

$http->listen($socket);

echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . PHP_EOL;

$loop->run();