<?php

namespace Narwhal\Bitbucket;

use Narwhal\Bitbucket\Exceptions\BitbucketException;



class Bitbucket
{
    /**
     * Set Json Setring
     *
     * @var string
     */
    protected $json = '';
    
    /**
     * Converted Json to Array
     *
     * @var array
     */
    protected $tokens = [];

    /**
     * Set Json in string.
     */
    public function __construct(string $json)
    {
        $this->tokens = json_decode($json);
    }

    /**
     * Get set Json String
     *
     * @return  string
     */ 
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Set set Json String
     *
     * @param  string  $json  Set Json Setring
     *
     * @return  self
     */ 
    public function setJson(string $json)
    {
        $this->json = $json;
    }

    
    public function handle()
    {
        if(empty($this->tokens)) {
            throw new BitbucketException('Invalid Json Data Received');
        }

    }
}