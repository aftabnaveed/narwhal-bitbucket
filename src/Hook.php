<?php

namespace Narwhal\Bitucket;

class Hook
{
    /**
     * Hook Type push|commit|comment etc.
     *
     * @var string
     */
    protected $type = 'push';

    /**
     * Repository the Hook is for.
     *
     * @var Repository
     */
    protected $repository = null;
    

    /**
     * Get the value of type
     */ 
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType(string $type) : void
    {
        $this->type = $type;
    }

    

    /**
     * Get repository the Hook is for.
     *
     * @return  string
     */ 
    public function getRepository() : Repository
    {
        return $this->repository;
    }

    /**
     * Set repository the Hook is for.
     *
     * @param  string  $repository  Repository the Hook is for.
     *
     * @return  self
     */ 
    public function setRepository(Repository $repository) : void
    {
        $this->repository = $repository;

    }
}