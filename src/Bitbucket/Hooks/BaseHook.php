<?php

namespace Narwhal\Bitucket\Hooks;

class BaseHook
{
    public function getOwner() : string
    {
        return '';
    }

    public function getRepository() {}
}