<?php

namespace Narwhal\Bitbucket\Exceptions;

class BitbucketException extends Exception {}