<?php

namespace Narwhal\Bitucket;

class Change
{

    /**
     * Forced true|false
     *
     * @var bool
     */
    protected $forced = false;

    /**
     * Type of Change new|old
     *
     * @var string
     */
    protected $changeType = ''; 

    /**
     * Branch the change was made in
     *
     * @var string
     */
    protected $branch = '';

    /**
     * Commit
     *
     * @var Commit
     */
    protected $commit = null;
    
    /**
     * Type of Change new|old
     *
     * @param string $changeType
     * @return void
     */
    public function setChangeType(string $changeType) : void
    {
        $this->changeType = $changeType;
    }

    public function getChangeType() : string
    {
        return $this->changeType;
    }

    public function setBranch(string $branch) : void
    {
        $this->branch = $branch;
    }

    public function getBranch() : string
    {
        return $this->branch;
    }


    /**
     * Get forced true|false
     *
     * @return  boolean
     */ 
    public function getForced() : bool
    {
        return $this->forced;
    }

    /**
     * Set forced true|false
     *
     * @param  boolean  $forced  Forced true|false
     *
     * @return  self
     */ 
    public function setForced(bool $forced)
    {
        $this->forced = $forced;

    }

    /**
     * Get commit
     *
     * @return  Commit
     */ 
    public function getCommit() : Commit
    {
        return $this->commit;
    }

    /**
     * Set commit
     *
     * @param  Commit  $commit  Commit
     *
     * @return  self
     */ 
    public function setCommit(Commit $commit) : void
    {
        $this->commit = $commit;
    }
}