<?php 

namespace Narwhal\Bitucket;

class Links
{
    /**
     * Self Href.
     *
     * @var string
     */
    protected $selfHref = '';

    /**
     * HTML Href.
     *
     * @var string
     */
    protected $htmlHref = '';

    /**
     * Commit's href.
     *
     * @var string
     */
    protected $commitsHref = '';

    /**
     * Get self Href.
     *
     * @return  string
     */ 
    public function getSelfHref()
    {
        return $this->selfHref;
    }

    /**
     * Set self Href.
     *
     * @param  string  $selfHref  Self Href.
     *
     * @return  self
     */ 
    public function setSelfHref(string $selfHref)
    {
        $this->selfHref = $selfHref;

        return $this;
    }

    /**
     * Get hTML Href.
     *
     * @return  string
     */ 
    public function getHtmlHref()
    {
        return $this->htmlHref;
    }

    /**
     * Set hTML Href.
     *
     * @param  string  $htmlHref  HTML Href.
     *
     * @return  self
     */ 
    public function setHtmlHref(string $htmlHref)
    {
        $this->htmlHref = $htmlHref;

        return $this;
    }

    /**
     * Get commit's href.
     *
     * @return  string
     */ 
    public function getCommitsHref()
    {
        return $this->commitsHref;
    }

    /**
     * Set commit's href.
     *
     * @param  string  $commitsHref  Commit's href.
     *
     * @return  self
     */ 
    public function setCommitsHref(string $commitsHref)
    {
        $this->commitsHref = $commitsHref;

        return $this;
    }
}