<?php

namespace Narwhal\Bitucket;

class Author extends Actor
{
    /**
     * Account info in raw format John Doe <john@example.com>
     *
     * @var string
     */
    protected $raw = '';

    /**
     * Get account info in raw format John Doe <john@example.com>
     *
     * @return  string
     */ 
    public function getRaw() : string
    {
        return $this->raw;
    }

    /**
     * Set account info in raw format John Doe <john@example.com>
     *
     * @param  string  $raw  Account info in raw format John Doe <john@example.com>
     *
     * @return  self
     */ 
    public function setRaw(string $raw) : void
    {
        $this->raw = $raw;
    }
}