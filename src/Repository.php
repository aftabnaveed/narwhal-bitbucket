<?php

namespace Narwhal\Bitucket;


class Repository
{
    /**
     * Repository type git|hg
     *
     * @var string
     */
    protected $scm = 'git';

    /**
     * Repository Name
     *
     * @var string
     */
    protected $name = '';

    /**
     * Repository Website
     *
     * @var string
     */
    protected $website = '';

    /**
     * Other links
     *
     * @var Links
     */
    protected $links = null;

    /**
     * Get repository type git|hg
     *
     * @return  string
     */ 
    public function getScm() : string
    {
        return $this->scm;
    }

    /**
     * Set repository type git|hg
     *
     * @param  string  $scm  Repository type git|hg
     *
     * @return  self
     */ 
    public function setScm(string $scm) : void
    {
        $this->scm = $scm;

        return $this;
    }

    /**
     * Get repository Name
     *
     * @return  string
     */ 
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Set repository Name
     *
     * @param  string  $name  Repository Name
     *
     * @return  self
     */ 
    public function setName(string $name) : void
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get repository Website
     *
     * @return  string
     */ 
    public function getWebsite() : string
    {
        return $this->website;
    }

    /**
     * Set repository Website
     *
     * @param  string  $website  Repository Website
     *
     * @return  self
     */ 
    public function setWebsite(string $website) : void
    {
        $this->website = $website;

    }

    /**
     * Get other links
     *
     * @return  Links
     */ 
    public function getLinks() : Links
    {
        return $this->links;
    }

    /**
     * Set other links
     *
     * @param  Links  $links  Other links
     *
     * @return  self
     */ 
    public function setLinks(Links $links) : void
    {
        $this->links = $links;
    }
}