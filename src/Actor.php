<?php

namespace Narwhal\Bitucket;

class Actor
{
    /**
     * Username associated with the hook
     *
     * @var string
     */
    protected $username = '';

    /**
     * Display Name of the user
     *
     * @var string
     */
    protected $displayName = '';

    /**
     * Account Id of the bitbucket user
     *
     * @var string
     */
    protected $accountId = '';

    /**
     * Links associated with the Account
     *
     * @var Links
     */
    protected $links = null;

    /**
     * Type of the actor
     *
     * @var string
     */
    protected $type = 'user';

    /**
     * User's uuid (bitbucket)
     *
     * @var string
     */
    protected $uuid = '';

    /**
     * Get username associated with the hook
     *
     * @return  string
     */ 
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * Set username associated with the hook
     *
     * @param  string  $username  Username associated with the hook
     *
     * @return  self
     */ 
    public function setUsername(string $username) : void
    {
        $this->username = $username;
    }

    /**
     * Get display Name of the user
     *
     * @return  string
     */ 
    public function getDisplayName() : string
    {
        return $this->displayName;
    }

    /**
     * Set display Name of the user
     *
     * @param  string  $displayName  Display Name of the user
     *
     * @return  self
     */ 
    public function setDisplayName(string $displayName) : void
    {
        $this->displayName = $displayName;

    }

    /**
     * Get account Id of the bitbucket user
     *
     * @return  string
     */ 
    public function getAccountId() : string
    {
        return $this->accountId;
    }

    /**
     * Set account Id of the bitbucket user
     *
     * @param  string  $accountId  Account Id of the bitbucket user
     *
     * @return  self
     */ 
    public function setAccountId(string $accountId) : void
    {
        $this->accountId = $accountId;
    }

    /**
     * Get links associated with the Account
     *
     * @return  Links
     */ 
    public function getLinks() : Links
    {
        return $this->links;
    }

    /**
     * Set links associated with the Account
     *
     * @param  Links  $links  Links associated with the Account
     *
     * @return  self
     */ 
    public function setLinks(Links $links) : void
    {
        $this->links = $links;
    }

    /**
     * Get type of the actor
     *
     * @return  string
     */ 
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * Set type of the actor
     *
     * @param  string  $type  Type of the actor
     *
     * @return  self
     */ 
    public function setType(string $type) : void
    {
        $this->type = $type;
    }

    /**
     * Get user's uuid (bitbucket)
     *
     * @return  string
     */ 
    public function getUuid() : string
    {
        return $this->uuid;
    }

    /**
     * Set user's uuid (bitbucket)
     *
     * @param  string  $uuid  User's uuid (bitbucket)
     *
     * @return  self
     */ 
    public function setUuid(string $uuid) : void
    {
        $this->uuid = $uuid;
    }
}