<?php

namespace Narwhal\Bitucket;

class Commit
{
    /**
     * Type of the old | new
     *
     * @var string
     */
    protected $type = '';

    /**
     * Commit hash
     *
     * @var string
     */
    protected $hash = '';

    /**
     * Commit Author
     *
     * @var string
     */
    protected $author = '';

    /**
     * Commit message
     *
     * @var string
     */
    protected $message = '';

    /**
     * Commit Message
     *
     * @var string
     */
    protected $date = '';

    /**
     * Commit parent.
     *
     * @var array
     */
    protected $parents = [];


    /**
     * Get type of the target | commit
     *
     * @return  string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * Set type of the target | commit
     *
     * @param  string  $type  Type of the target | commit
     *
     * @return  self
     */
    public function setType(string $type) : void
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get commit hash
     *
     * @return  string
     */
    public function getHash() : string
    {
        return $this->hash;
    }

    /**
     * Set commit hash
     *
     * @param  string  $hash  Commit hash
     *
     * @return  self
     */
    public function setHash(string $hash) : void
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get commit Author
     *
     * @return  string
     */
    public function getAuthor() : string
    {
        return $this->author;
    }

    /**
     * Set commit Author
     *
     * @param  string  $author  Commit Author
     *
     * @return  self
     */
    public function setAuthor(string $author) : void
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get commit message
     *
     * @return  string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * Set commit message
     *
     * @param  string  $message  Commit message
     *
     * @return  self
     */
    public function setMessage(string $message) : void
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get commit Message
     *
     * @return  string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set commit Message
     *
     * @param  string  $date  Commit Message
     *
     * @return  self
     */
    public function setDate(string $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get commit parent.
     *
     * @return  array
     */
    public function getParents() : array
    {
        return $this->parents;
    }

    /**
     * Set commit parent.
     *
     * @param  array  $parents  Commit parent.
     *
     * @return  self
     */
    public function setParents(array $parents) : void
    {
        $this->parents = $parents;

        return $this;
    }
}
